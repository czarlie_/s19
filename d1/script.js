console.log(`*** JS ES6 Updates ***`)

/*
1) Exponent Operator
*/

// Previous Version:
const firstNum = 8 ** 2
console.log(firstNum)

// ES6 Version:
const secondNum = Math.pow(8, 2)
console.log(secondNum)

/*
2) Template Literals (``)
        - Allows to write strings without using
        the concatenation operator (+)
        - Greatly helps with code readability
*/

let name = `John`

let message = 'Hello ' + name + '! Welcome to programming!!!'
console.log('Message without the template literals: ' + message)

message = `Hello ${name}! Welcome to programming!!!`
console.log(`Message without the template literals: ${message}`)

const anotherMessage = `
${name} attended a Math competition.
He won it by solving the problem 8 ** 2 with the 
            solution of ${firstNum}`
console.log(anotherMessage)

const errorMessage = `
*================================*
|                                |
| ERROR!!! Enter a valid number. |
|                                |
*================================*`
console.log(errorMessage)

const interestRate = .1
const principal = 1000
console.log(`The interest on your savings account is: ${principal * interestRate}`)

/*
3) Array Destructuring
        - Allows to unpack elements in arrays
             into distinct variables
        - Allows us to name array elements
            with variables instead of using index
            numbers
*/

const fullName = ['Juan', 'Dela', 'Cruz']

console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)
const [firstName, middleName, lastName] = fullName

// ES6 - Destructuring
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

/*
4) Object Destructuring
        - Allows to unpack propeties of an object
        into distinct variables
*/

const person = {
    givenName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz',
    walk: () => {
    return `${gName} walked 1km.`}
}

// Prev:
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

// ES6: Destructuring
const { givenName, maidenName, walk, familyName } = person
console.log(givenName)
console.log(maidenName)
console.log(familyName)
console.log(walk)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`)

/*
5) Arrow Function
    - Compact alternative syntax to additional functions
    - Useful for code snippets where creating functions
        will not be reused in any other portion of the code
SYNTAX: const variableName = (parameters) => {statements}
*/

// PV:
const hello = function() {
    console.log(`Hello World!`)
}

// ES6
const helloAgain = () => {
    console.log(`Hello World!`)
}

hello()
helloAgain()

// Arrow functions with loops
const students = ['John', 'Jane', 'Judy']

// PV:
students.forEach(function(student) {
    console.log(`${student} is a student.`)
})

// ES6
students.forEach((student) => {
    console.log(`${student} is a student.`)
})

/*
6) Implicit Return Statement
    - There are some instances when you can omitt
        the "return statement"
    - Javascript implicity adds it for the result
        of the function
*/
// PV
function multiplication (x, y) {
    return (x * y)
}
let product = multiplication(1, 2)
console.log(product)

// ES6
const addition = (x, y) => x + y
let total = addition(1, 2)
console.log(total)

/*
7) Default Function Argument Value
    - Provides a default argument value if
        none is provided
*/

const greet = (name = 'User') => {
    return `Good morning, ${name}!`
}

console.log(greet())
console.log(greet("John"))

/*
8) Class-Based Object Blueprint
    - Allows creation/installation of objects
        using classes as blueprints

Creating a Class
    - The constructor is a specil method of a class
        for that class
*/

class Car {
    constructor(brand, name, year) {
        this.brand = brand
        this.year = year
        this.name = name        
    }
}

const myCar = new Car()
console.log(myCar)

myCar.brand = "Ford"
myCar.name = "Ranger Raptop"
myCar.year = 2021
console.log(myCar)

// Creating new instance of car with initialized values
const myNewCar = new Car('Toyota', 'Vios', 2021)
console.log(myNewCar)